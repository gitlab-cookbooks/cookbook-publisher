ROOT_DIR    := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BUNDLE_PATH ?= $(ROOT_DIR)/.bundle

.PHONY: gems
gems:
	gem install bundler -v $(BUNDLER_VERSION)
	bundle config set clean 'true'
	bundle config set path $(BUNDLE_PATH)
	bundle config set --local frozen 'true'
	bundle install --jobs $$(nproc)

.PHONY: check
check:
	bundle exec rubocop

.PHONY: rspec
rspec: check
	bundle exec rspec -f d
