#!/usr/bin/env ruby

# frozen_string_literal: true

require 'fileutils'
require 'base64'
require 'English'
require 'net/http'
require 'json'

CHEF_REPO_PATH = '/tmp/chef-repo'

# MetadataEvaluator
class MetadataEvaluator
  def initialize(metadata_content)
    instance_eval(metadata_content)
  end

  # rubocop:disable Naming/AccessorMethodName
  def get_version
    @version && @version[0]
  end

  def get_name
    @name[0]
  end
  # rubocop:enable Naming/AccessorMethodName

  private

  def method_missing(meth, *args)
    instance_variable_set("@#{meth}", args)
  end
end

def log(msg)
  puts "[PUBLISHER] #{msg}"
end

def run_cmd(args, mask: {}, allow_failure: false, stdin: nil)
  output = nil

  # If we're running from a Bundler context, assume we also want to use the bundled berks executable
  args = %w[bundle exec] + args if !ENV['BUNDLE_GEMFILE'].nil? && (args[0] == 'berks')

  masked_args = args.dup
  mask.each do |i, masked_value|
    masked_args[i] = masked_value
  end

  log "Running command: #{masked_args}"
  IO.popen(args, 'r+') do |cmd_io|
    if stdin
      cmd_io.puts(stdin)
      cmd_io.close_write
    end

    output = cmd_io.read
  end

  exit_status = $CHILD_STATUS.exitstatus
  if !allow_failure && exit_status != 0
    log "[FAIL] Command execution failed. Exit status = #{exit_status}"
    exit 1
  end

  output
end

def in_chef_repo(&block)
  Dir.chdir(CHEF_REPO_PATH, &block)
end

def evaluate_metadata
  old_metadata = run_cmd(%w[git show HEAD^1:metadata.rb])
  new_metadata = run_cmd(%w[git show HEAD:metadata.rb])

  [
    MetadataEvaluator.new(old_metadata),
    MetadataEvaluator.new(new_metadata)
  ]
end

def check_versions(old_metadata, new_metadata)
  old_version = old_metadata.get_version
  new_version = new_metadata.get_version

  log "Old version: #{old_version}"
  log "New version: #{new_version}"

  return if new_version != old_version || ENV['COOKBOOK_PUBLISHER_SKIP_VERSION_CHECK'] == 'true'

  log 'Same versions, nothing to do'
  exit 0
end

def install_deps
  berks_installed =
    begin
      run_cmd(%w[berks --help], allow_failure: false)
      true
    rescue StandardError
      false
    end
  unless berks_installed
    # semverse >= 3.0.2 requires ruby 2.7 so fixing it to 3.0.0, which works with ruby 2.6
    run_cmd(%w[gem install semverse -v 3.0.0])
    # train-core > 3.9.2 does not support our combination of ruby/rubygems so pinning it :(
    run_cmd(%w[gem install train-core -v 3.9.2])
    # nori > 2.6.0 requires Ruby 3.0, so we pin it to the version that works with Ruby 2.6
    run_cmd(%w[gem install nori -v 2.6.0])

    log 'installing chef ...'
    run_cmd(%w[gem install chef -v 16.13.16])
    log 'installing berkshelf ...'
    run_cmd(%w[gem install berkshelf -v ~>7.0.0])
  end
  begin
    require 'gitlab'
  rescue LoadError
    log 'installing gitlab ...'
    run_cmd(%w[gem install gitlab])
    # Force ruby to re-evaluate available gems
    Gem.clear_paths
    require 'gitlab'
  end
end

def populate_knife_config
  chef_conf_dir = "#{Dir.home}/.chef"

  log "Populating knife config in #{chef_conf_dir} ..."

  FileUtils.mkdir_p(chef_conf_dir)

  log 'Writing knife.rb ...'
  File.write("#{chef_conf_dir}/knife.rb", <<~STR
    current_dir = File.dirname(__FILE__)
    log_level                :debug
    log_location             STDOUT
    node_name                "publisher"
    client_key               "\#{current_dir}/publisher.pem"
    chef_server_url          "#{ENV.fetch('CHEF_SERVER_URL')}"
  STR
  )
  log 'Done'

  log 'Writing publisher.pem ...'
  File.write("#{chef_conf_dir}/publisher.pem", ENV.fetch('COOKBOOK_PUBLISHER_PRIVATE_KEY'))
  log 'Done'
end

def use_stable_rubygems
  rubygems_version = run_cmd(%w[gem --version])
  if Gem::Version.new(rubygems_version) > Gem::Version.new('2.7.5')
    log "RubyGems version #{rubygems_version} is newer than 2.7.5. Skipped install of RubyGems version 2.7.5."
  else
    # Work around https://github.com/berkshelf/berkshelf/issues/1744
    run_cmd(%w[gem update --system 2.7.5])
  end
end

def setup_ops_ssh
  run_cmd(%w[ssh-agent -c]).each_line do |line|
    next unless line =~ /^setenv/

    _, key, value = line.split
    ENV[key] = value.sub(/;$/, '')
  end

  private_key = Base64.decode64(ENV.fetch('CI_PRIVATE_KEY'))
  run_cmd(%w[ssh-add -], stdin: private_key)

  ssh_config_path = "#{Dir.home}/.ssh/config"

  log "Creating #{File.dirname(ssh_config_path)}"
  FileUtils.mkdir_p(File.dirname(ssh_config_path))

  log 'Disabling StrictHostKeyChecking'
  File.write(ssh_config_path, "Host *\n\tStrictHostKeyChecking no\n\n")
end

def upload_cookbook(metadata)
  cookbook_name = metadata.get_name

  run_cmd(%W[git clone #{ENV.fetch('CHEF_REPO_CLONE_URL')} #{CHEF_REPO_PATH}], mask: { 2 => 'chef-repo URL' })

  in_chef_repo do
    if ENV['BUNDLE_GEMFILE'].nil?
      # If running from a bundler context, reinstalling bundle will unload bundled gems.

      # Ensure the right Bundler version is installed to avoid `Gem::MissingSpecError` exception.
      # https://bundler.io/blog/2019/05/14/solutions-for-cant-find-gem-bundler-with-executable-bundle.html
      chef_repo_bundler_version = `grep -A 1 "BUNDLED WITH" Gemfile.lock | tail -n 1`
      chef_repo_bundler_version.chomp! # Remove `/n`
      run_cmd(%W[gem install bundler -v #{chef_repo_bundler_version}])
    end
    run_cmd(%W[berks update #{cookbook_name}])
    run_cmd(%w[berks upload])
  end
end

def update_environment_files(envs, metadata)
  commit_messages = run_cmd(%w[git log HEAD^1..HEAD --format=format:%B]).force_encoding('utf-8')
  commit_exclusions = commit_messages.scan(/\[skip-(?:#{envs.join('|')})\]/)
  project_exclusions = ENV['COOKBOOK_PUBLISHER_SKIP_ENVS'].to_s.split(',')

  cookbook_name = metadata.get_name

  in_chef_repo do
    envs.each do |environment|
      skip_tag = "[skip-#{environment}]"
      if commit_exclusions.include?(skip_tag)
        log "#{skip_tag} found, skipping updating environment file"
        next
      end

      if project_exclusions.include?(environment)
        log "#{environment} found in COOKBOOK_PUBLISHER_SKIP_ENVS, skipping updating environment file"
        next
      end

      file_path = File.join('environments', "#{environment}.json")
      content   = File.read(file_path)

      content.gsub!(
        /"#{cookbook_name}":\s*"(\W+)[0-9.]+"/,
        "\"#{cookbook_name}\": \"\\1#{metadata.get_version}\""
      )

      log "Updating #{file_path} ..."
      File.write(file_path, content)
    end
  end
end

# rubocop:disable Metrics/MethodLength, Metrics/AbcSize
def create_chef_repo_merge_request(envs, metadata, suffix = '')
  if envs.empty?
    log 'Environments array is empty'
    return
  end

  log "Processing environments: #{envs.join ', '}"

  update_environment_files(envs, metadata)

  commit_message = "Bump #{metadata.get_name} version to #{metadata.get_version} #{suffix}".strip
  branch_name = commit_message.gsub(/\W/, '-').downcase

  # Get last merge commit link, which likely bumped cookbook version
  last_merge_commit_hash = run_cmd(%w[git log --merges -n 1 --pretty=format:%H])
  link_to_cookbook_merge_commit = "#{ENV.fetch('CI_PROJECT_URL')}/-/commit/#{last_merge_commit_hash}"

  in_chef_repo do
    if run_cmd(%w[git status --short]).strip == ''
      log 'No changes were found'
      return
    end

    run_cmd(%w[git checkout master])
    run_cmd(%W[git checkout -b #{branch_name}])
    run_cmd(%w[git add .])
    run_cmd(['git', '-c', 'user.email=ops-notifications+cookbook-publisher@gitlab.com', '-c', 'user.name="Cookbook Publisher"', 'commit', '-m', commit_message], allow_failure: true)
    run_cmd(%w[git push origin HEAD])
  end

  cookbook_client = nil
  cc_users = []
  mrs = []
  source_repo_prat = ENV.fetch('GITLAB_SOURCE_REPO_PRAT', nil)
  if source_repo_prat
    log 'Found a PrAT!'

    cookbook_client = Gitlab.client(
      endpoint: 'https://gitlab.com/api/v4',
      private_token: source_repo_prat
    )
    source_project = cookbook_client.project(ENV.fetch('CI_PROJECT_PATH'))

    log "Getting a list of MRs for commit '#{last_merge_commit_hash}'"
    commit_mrs = cookbook_client.commit_merge_requests(source_project.id, last_merge_commit_hash)
    commit_mrs.each do |mr|
      log "Found MR: #{mr.web_url}"
      cc_users << "@#{mr.author.username}"
      mrs << [source_project.id, mr]
    end
  else
    log 'No PrAT - fallback to using "GITLAB_USER_LOGIN"'
    cc_users << "@#{ENV.fetch('GITLAB_USER_LOGIN')}"
  end

  chef_repo_client = Gitlab.client(
    endpoint: 'https://gitlab.com/api/v4',
    private_token: ENV.fetch('COOKBOOK_PUBLISHER_API_TOKEN')
  )
  log 'Creating merge request in chef-repo ...'
  chef_repo_mr = chef_repo_client.create_merge_request(
    ENV.fetch('CHEF_REPO_PROJECT_ID'),
    commit_message,
    {
      'source_branch' => branch_name,
      'target_branch' => 'master',
      'description' =>
        "## MR checklist (Don't remove):\n" \
        "- [ ] Verified [last MR commit of cookbook](#{link_to_cookbook_merge_commit}) to see all the potential changes going to be applied\n\n" \
        "cc/ #{cc_users.join(' ')}\n\n" \
        'Note: To improve this auto-generated MR issue description, visit https://gitlab.com/gitlab-cookbooks/cookbook-publisher/',
      'labels' => 'backstage',
      'remove_source_branch' => true
    }
  )

  # Add a note to the cookbook MR(s) linking to the chef-repo MR
  envs_formatted = envs.map { |env| "`#{env}`" }.join ', '
  mrs.each do |project_id, mr|
    log "Adding a note in MR: #{mr.web_url}"
    cookbook_client.create_merge_request_note(project_id, mr.iid, "Please review the following chef-repo MR for environments #{envs_formatted}: #{chef_repo_mr.web_url}")
  end
end
# rubocop:enable Metrics/MethodLength, Metrics/AbcSize

def find_environments
  glob = File.join(CHEF_REPO_PATH, 'environments/*.json')
  env_files = Dir.glob(glob)
  if env_files.empty?
    log "[FAIL] Unable to find environment json files in '#{glob}'"
    exit 1
  end
  envs = []
  env_files.each do |file|
    json = JSON.parse(File.read(file))
    env_name = json['name']
    if env_name.nil?
      log "[FAIL] Unable to read env name from '#{file}'"
      exit 1
    end
    envs.push(env_name)
  end
  envs.sort
end

if $PROGRAM_NAME == __FILE__
  # Environments specified here will be put in a separate MR from the rest
  production_envs = ENV.fetch('COOKBOOK_PUBLISHER_PRODUCTION_ENVS', '').split(',')
  old_metadata, new_metadata = evaluate_metadata
  check_versions(old_metadata, new_metadata)
  setup_ops_ssh
  use_stable_rubygems
  install_deps
  populate_knife_config
  upload_cookbook(new_metadata)

  create_chef_repo_merge_request(find_environments - production_envs, new_metadata)
  create_chef_repo_merge_request(production_envs, new_metadata, 'in production')
end
