# frozen_string_literal: true

require 'spec_helper'
require 'gitlab'
require 'hash_dot'

require_relative '../publisher'

describe Object do
  before do
    stub_const('CHEF_REPO_PATH', '/tmp')
    stub_const('ENV', { 'CI_PROJECT_URL' => 'https://gitlab.com/some/project' })

    allow_any_instance_of(described_class)
      .to receive(:update_environment_files)
      .with(anything, anything)
  end

  context 'when creating a new chef-repo merge request' do
    let(:metadata) do
      MetadataEvaluator.new(%(
        # frozen_string_literal: true

        name             'packagename'
        maintainer       'GitLab Inc.'
        maintainer_email 'email@address.com'
        license          'MIT'
        description      'Installs/Configures packagename'
        version          '0.0.1'
        chef_version     '>= 14.0'

        depends 'foo', '0.0.1'
        depends 'bar'
        ))
    end

    it 'returns early when no environments are found' do
      expect { create_chef_repo_merge_request([], metadata) }
        .to output(/is empty/)
        .to_stdout
    end

    it 'exits cleanly when no changes are found' do
      allow_any_instance_of(described_class)
        .to receive(:run_cmd)
        .and_return('hash', '')

      expect { create_chef_repo_merge_request(%w[one two], metadata) }
        .to output(/no changes were found/i)
        .to_stdout
    end

    it 'creates a merge request and an MR note' do
      stub_const('ENV', ENV.to_hash.merge({
                                            'GITLAB_SOURCE_REPO_PRAT' => 'token123',
                                            'CHEF_REPO_PROJECT_ID' => 'chef_repo_123',
                                            'CI_PROJECT_PATH' => 'project/path',
                                            'COOKBOOK_PUBLISHER_API_TOKEN' => 'publisher-token123'
                                          }))

      gitlab_client = double
      allow(Gitlab).to receive(:client).with(anything).and_return(gitlab_client)
      allow(gitlab_client).to receive(:project).with(anything).and_return({ id: 123 }.to_dot)
      allow(gitlab_client).to receive(:commit_merge_requests).with(123, 'hash').and_return([{
        iid: 456,
        web_url: 'https://gitlab.com/some-project/-/merge-requests/123',
        author: { username: 'username' }
      }.to_dot])
      allow(gitlab_client).to receive(:create_merge_request).with('chef_repo_123', 'Bump packagename version to 0.0.1', anything).and_return({ web_url: 'https://foo.com' }.to_dot)
      allow(gitlab_client).to receive(:create_merge_request_note).with(123, 456, anything)

      allow_any_instance_of(described_class)
        .to receive(:run_cmd)
        .and_return('hash', 'status', '')

      expect { create_chef_repo_merge_request(%w[env1 env2], metadata) }
        .to output(/found a prat/i)
        .to_stdout
    end
  end
end
